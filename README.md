# EOS GRPC-GATEWAY

The EOS-GRPC-GATEWAY is part of the project EOS HTTP REST API. The aim of the project is to provide an easy-to-maintain HTTP REST API to the existing commands in EOS. The project adds a very thin layer, the eos-grpc-gateway, that does the translation from HTTP REST API requests to GRPC requests.

## Project architecture

The main building blocks:
* XrdHttp plugin capable of receiving HTTP REST API requests and of forwarding them to the GRPC-GATEWAY;
* EOS-GRPC-GATEWAY, the go translation unit to convert such requests to GRPC request;
* GrpcRestGw Server, the GRPC service that contains the implementation of the EOS console commands and is able to execute the requests inside the EOS MGM.

In order to keep this functionality inside the EOS MGM without adding an external server for the translation unit, the GRPC-GATEWAY is packaged as a separate C++ library that can be linked to the GrpcRestGw server.

The architecture of the project is summarised in the following diagram:

![Alt text](readme_diagram.png)

## Project workflow

In order to access EOS console commands through HTTP requests, an HTTP client has to send a request on the XrdHttp port of EOS MGM. The XrdHttp plugin creates a curl request and forwards it to the EOS GRPC-GATEWAY. The gateway translates the HTTP request into a grpc request and sends it to the GrpcRestGw server. The server handles the request and sends back the response, which is again translated on the way back to the client.

All of the steps described previously are taking place inside the EOS MGM.

## Documentation
The project documentation is generated automatically from the EOS console commands protobuf definitions using OpenAPI and the protoc-gen-swagger plugin. OpenAPI describes the json format of the body that needs to be passed by the users in their HTTP request for a certain EOS console command.

The documentation can be accessed at the following link on the SwaggerHub platform:
[EOS-HTTP-REST-GW-API](https://app.swaggerhub.com/apis/ANDREEAPRIGOREANU/EOS-HTTP-REST-GW/1.0)

## Current limitations

The EOS-grpc-gateway provided by the current project and the GrpcRestGw server located in the EOS MGM are generated from the same protobuf definitions. For the time being, the proto files exist in both projects and must be kept in sync.
