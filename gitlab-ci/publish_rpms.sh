#!/usr/bin/env bash

EOS_CODENAME="diopside"
STCI_ROOT_PATH="/eos/project/s/storage-ci/www/eos"

for BUILD_TYPE in "el-7" "el-8" "el-9" "fc-38"; do
    EXPORT_DIR_RPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/x86_64/
    echo "Publishing for: ${BUILD_TYPE} in location: ${EXPORT_DIR_RPMS}"
    mkdir -p ${EXPORT_DIR_RPMS}
    cp ${BUILD_TYPE}/RPMS/x86_64/*.rpm ${EXPORT_DIR_RPMS}
    createrepo -q ${EXPORT_DIR_RPMS}
done

if [[ -n $(find el-9-arm/RPMS/aarch64 -name *.rpm -type f) ]]; then
    BUILD_TYPE="el-9-arm"
    EXPORT_DIR_RPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/el-9/aarch64/
    echo "Publishing for: ${BUILD_TYPE} in location: ${EXPORT_DIR_RPMS}"
    mkdir -p ${EXPORT_DIR_RPMS}
    cp  ${BUILD_TYPE}/RPMS/aarch64/*.rpm ${EXPORT_DIR_RPMS}
    createrepo -q ${EXPORT_DIR_RPMS}
fi
