%global debug_package %{nil}

Name:          eos-grpc-gateway
Version:       @SPEC_VERSION@
Release:       1%{?dist}
Summary:       EOS grpc-gateway

License:       none
URL:           https://gitlab.cern.ch/eos/grpc-gateway
Source:        %{name}-%{version}.tar.gz

BuildRequires: golang >= 1.19
BuildRequires: eos-grpc, eos-grpc-devel, eos-grpc-plugins
BuildRequires: gcc, gcc-c++
BuildRequires: gflags, gflags-devel
BuildRequires: tar
BuildRequires: cmake >= 3.17

# Don't put an explicit dependency on libraries provided by eos-grpc because
# the package hides shared libraries from the list of provided capabilities.
%global __requires_exclude %{__requires_exclude}|^libprot.*$|^libabsl.*$|^libgrpc.*$|^libaddress_sorting.*$|^libre2.*$|^libgpr.*$|^libupb.*$


%description
The EOS grpc-gateway receives HTTP requests for EOS console commands, translates the HTTP requests to gRPC requests and forwards them to the EOS gRPC server.


%prep
%setup -q


%build
echo "Installing go plugins..."
go get \
   github.com/grpc-ecosystem/grpc-gateway/v2/internal/descriptor@v2.16.2 \
   github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@v2.16.2 \
   github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2/internal/genopenapi@v2.16.2 \
   github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
   google.golang.org/grpc/cmd/protoc-gen-go-grpc

go install \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
    github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
    google.golang.org/protobuf/cmd/protoc-gen-go \
    google.golang.org/grpc/cmd/protoc-gen-go-grpc

echo "Create build directory"
mkdir -p build
echo "Enter build directory"
cd build
echo "Build project..."
cmake ../ -DPROTOBUF_ROOT=/opt/eos/grpc/
make %{_smp_mflags}
echo "Building done!"


%install
echo "Installing files..."
cd build
make install DESTDIR=$RPM_BUILD_ROOT
echo "Installed!"


%files
%doc build/eos_rest_gateway_service.swagger.json
%{_libdir}/libEosGrpcGateway.so
%{_includedir}/EosGrpcGateway.h


%changelog
* Tue Sep 05 2023 - 1.0
- Initial release
