# Try to find PROTOBUF3
# Once done, this will define
#
# PROTOBUF3_FOUND               - system has Protobuf3
# PROTOBUF3_INCLUDE_DIRS        - Protobuf3 include directories
# PROTOBUF3_LIBRARIES           - libraries needed to use Protobuf3
#
# and the following imported targets
#
# PROTOBUF::PROTOBUF
#
# PROTOBUF_ROOT may be defined as a hint for where to look

find_program(PROTOBUF3_PROTOC_EXECUTABLE
  NAMES protoc
  HINTS ${PROTOBUF_ROOT}
  PATHS /opt/eos/grpc/ /opt/eos/ /usr/local /usr /
  PATH_SUFFIXES bin
  DOC "Version 3 of The Google Protocol Buffers Compiler (protoc)"
  NO_DEFAULT_PATH)

find_path(PROTOBUF3_INCLUDE_DIR
  NAMES google/protobuf/message.h
  HINTS ${PROTOBUF_ROOT}
  PATHS /opt/eos/include/protobuf3 /usr/include/protobuf3 /usr/local /usr
  PATH_SUFFIXES include
  NO_DEFAULT_PATH)

find_library(PROTOBUF3_LIBRARY
  NAME protobuf
  HINTS ${PROTOBUF_ROOT}
  PATHS /opt/eos/lib64/protobuf3 /usr/lib64/protobuf3 /usr/lib/protobuf3
	/usr/local /usr /usr/lib/x86_64-linux-gnu
  PATH_SUFFIXES lib64 lib
  NO_DEFAULT_PATH)

find_program(PROTOC_GEN_GRPC_GATEWAY
  NAME protoc-gen-grpc-gateway
  HINTS ${PROTOBUF_ROOT}
  PATHS ${GOPATH}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-grpc-gateway was found
if (PROTOC_GEN_GRPC_GATEWAY)
  message(STATUS "Found protoc-gen-grpc-gateway: ${PROTOC_GEN_GRPC_GATEWAY}")
else()
  message(STATUS "protoc-gen-grpc-gateway not found")
endif()

find_program(PROTOC_GEN_OPENAPIV2
  NAME protoc-gen-openapiv2
  HINTS ${PROTOBUF_ROOT}
  PATHS ${GOPATH}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-openapiv2 was found
if (PROTOC_GEN_OPENAPIV2)
  message(STATUS "Found protoc-gen-openapiv2: ${PROTOC_GEN_OPENAPIV2}")
else()
  message(STATUS "protoc-gen-openapiv2 not found")
endif()
 
find_program(PROTOC_GEN_GO
  NAME protoc-gen-go
  HINTS ${PROTOBUF_ROOT}
  PATHS ${GOPATH}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-go was found
if (PROTOC_GEN_GO)
  message(STATUS "Found protoc-gen-go: ${PROTOC_GEN_GO}")
else()
  message(STATUS "protoc-gen-go not found")
endif()

find_program(PROTOC_GEN_GO_GRPC
  NAME protoc-gen-go-grpc
  HINTS ${PROTOBUF_ROOT}
  PATHS ${GOPATH}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-go-grpc was found
if (PROTOC_GEN_GO_GRPC)
  message(STATUS "Found protoc-gen-go-grpc: ${PROTOC_GEN_GO_GRPC}")
else()
  message(STATUS "protoc-gen-go-grpc not found")
endif()

find_program(PROTOC_GEN_SWAGGER
  NAME protoc-gen-swagger
  HINTS ${PROTOBUF_ROOT}
  PATHS ${GOPATH}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-go-grpc was found
if (PROTOC_GEN_SWAGGER)
  message(STATUS "Found protoc-gen-swagger: ${PROTOC_GEN_SWAGGER}")
else()
  message(STATUS "protoc-gen-swagger not found")
endif()

find_program(GRPC_CPP_PLUGIN
  NAME grpc_cpp_plugin
  HINTS ${PROTOBUF_ROOT}
  PATHS /opt/eos/grpc
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH)

# check if protoc-gen-go-grpc was found
if (GRPC_CPP_PLUGIN)
  message(STATUS "Found grpc_cpp_plugin: ${GRPC_CPP_PLUGIN}")
else()
  message(STATUS "grpc_cpp_plugin not found")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Protobuf3
  REQUIRED_VARS PROTOBUF3_LIBRARY PROTOBUF3_INCLUDE_DIR PROTOBUF3_PROTOC_EXECUTABLE)
mark_as_advanced(PROOBUF3_FOUND PROTOBUF3_INCLUDE_DIR PROTOBUF3_LIBRARY
  PROTOBUF3_PROTOC_EXECUTABLE)

if (PROTOBUF3_FOUND AND NOT TARGET PROTOBUF::PROTOBUF)
  # These are set for make the find_package(Protobuf) happy at the end and
  # at the same time include the PROTOBUF_GENERATE_CPP function
  set(Protobuf_FOUND ${PROTOBUF3_FOUND})
  set(Protobuf_INCLUDE_DIR ${PROTOBUF3_INCLUDE_DIR})
  set(Protobuf_LIBRARY ${PROTOBUF3_LIBRARY})
  set(Protobuf_PROTOC_EXECUTABLE ${PROTOBUF3_PROTOC_EXECUTABLE})

  add_library(PROTOBUF::PROTOBUF UNKNOWN IMPORTED)
  set_target_properties(PROTOBUF::PROTOBUF PROPERTIES
    IMPORTED_LOCATION "${PROTOBUF3_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${PROTOBUF3_INCLUDE_DIR}")

  # Overwrite these since they are used in generating the Protobuf files
  if (NOT TARGET protobuf::protoc)
    add_executable(protobuf::protoc IMPORTED)
  endif()

  set_target_properties(protobuf::protoc PROPERTIES
    IMPORTED_LOCATION ${PROTOBUF3_PROTOC_EXECUTABLE})

  if (NOT TARGET protobuf::libprotobuf)
    add_library(protobuf::libprotobuf UNKNOWN IMPORTED)
  endif()

  set_target_properties(protobuf::libprotobuf PROPERTIES
   INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF3_INCLUDE_DIR}
   IMPORTED_LOCATION ${PROTOBUF3_LIBRARY})
endif ()

# Include Protobuf package from the generation commands like PROTOBUF_GENERATE_CPP
find_package(Protobuf)
