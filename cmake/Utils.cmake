# function to search for GOPATH value
function(GET_GOPATH GOPATH)
    execute_process(
        COMMAND go env GOPATH
        OUTPUT_VARIABLE GOPATH_OUTPUT
        RESULT_VARIABLE RETC
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    
    set(${GOPATH} ${GOPATH_OUTPUT} PARENT_SCOPE)
    
    if(NOT ("${RETC}" STREQUAL "0") )
        message(FATAL_ERROR "Error calling command \"go env GOPATH\", return code is ${RETC}")
    endif()

endfunction()

